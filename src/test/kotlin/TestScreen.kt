import com.codeborne.selenide.WebDriverRunner.url
import driverPack.DriverInitialize
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource
import sitePage.TextBoxPage

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@DisplayName("Форма ввода")
class TestScreen {

    private val textBoxPage: TextBoxPage = TextBoxPage()

    @BeforeAll
    fun initDriver() {
        DriverInitialize().startBrowser("chrome")
    }

    @BeforeEach
    fun beforeEach() {
        textBoxPage.openPage()
    }

    @Test
    @DisplayName("Открытие страницы")
    fun openTextBoxPage() {
        assertEquals("https://demoqa.com/text-box", url())
    }

    @Nested
    @DisplayName("Проверка поля ФИО")
    inner class TestFullName {

        @ParameterizedTest
        @ValueSource(strings = ["Gregozh Bzhenschekevich", "Артур Пирожков", "Абдуль-Хам Усад Николевич", "12345"])
        @DisplayName("Проверка ФИО")
        fun inputCorrectFullName(input: String) {
            textBoxPage.inputFullName(input)
                .clickSubmitButton()
            assertEquals("Name:$input", textBoxPage.getFinalName())
        }

    }

}