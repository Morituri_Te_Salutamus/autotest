package driverPack

import com.codeborne.selenide.SelenideElement

open class BaseClass {

    fun clickButton(element: SelenideElement) {
        scrollToElement(element).click()
    }

    fun inputText(text: String, element: SelenideElement) {
        scrollToElement(element).value = text
    }

    private fun scrollToElement(element: SelenideElement): SelenideElement {
        element.scrollTo()
        return element
    }
}