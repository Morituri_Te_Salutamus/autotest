package driverPack

import com.codeborne.selenide.Configuration
import io.github.bonigarcia.wdm.WebDriverManager

class DriverInitialize {
    fun startBrowser(browserName: String) {
        when (browserName) {
            "chrome" -> WebDriverManager.chromedriver().setup()
            "opera" -> WebDriverManager.operadriver().setup()
            else -> println("Browser not defined")
        }
        setConfiguration()
    }

    private fun setConfiguration() {
        Configuration.browser = "chrome"
        Configuration.headless = false
    }


}