package sitePage

import com.codeborne.selenide.Selenide.element
import com.codeborne.selenide.Selenide.open
import driverPack.BaseClass
import org.openqa.selenium.By

class TextBoxPage : BaseClass() {

//    private val base = BaseClass()

    private val urlAddress: String = "https://demoqa.com/text-box"
    private val fullName by lazy { element(By.id("userName")) }
    private val userEmail by lazy { element(By.id("userEmail")) }
    private val currentAddress by lazy { element(By.id("currentAddress")) }
    private val permanentAddress by lazy { element(By.id("permanentAddress")) }
    private val buttonSubmit by lazy { element(By.id("submit")) }
    private val finalName by lazy { element(".mb-1") }

    //Открытие страницы
    fun openPage() {
        open(urlAddress)
    }

    //Получить финальное ФИО
    fun getFinalName(): String {
        return finalName.text()
    }

    //Нажать на кнопку Подтвредить
    fun clickSubmitButton(): TextBoxPage {
        clickButton(buttonSubmit)
        return this
    }

    //Ввод в поле ФИО
    fun inputFullName(text: String): TextBoxPage {
        inputText(text, fullName)
        return this
    }

    //Ввод в поле Почта
    fun inputUserEmail(text: String): TextBoxPage {
        inputText(text, userEmail)
        return this
    }

    //Ввод в поле Текущий адрес
    fun inputCurrentAddress(text: String): TextBoxPage {
        inputText(text, currentAddress)
        return this
    }

    //Ввод в поле Адрес регистрации
    fun inputPermanentAddress(text: String): TextBoxPage {
        inputText(text, permanentAddress)
        return this
    }

}


